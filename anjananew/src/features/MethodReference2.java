package features;

import java.util.ArrayList;
import java.util.List;

public class MethodReference2 {

	public static void main(String[] args) {
		List<String> demo =new ArrayList<>();
		demo.add("Apple");
		demo.add("Banana");
		demo.add("Grape");
		demo.add("Pine Apple");
		
		demo.forEach(name->System.out.println(name));
		

	}

}
