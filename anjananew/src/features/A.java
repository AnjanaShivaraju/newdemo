package features;

interface MyInterface{
	void method1();
	static void method2() 
	{
		System.out.println( " This is static method logic..");
	}
	 default void method3()
	 {
		 System.out.println(" This is default method..");
	 }
}
	


public class A  implements MyInterface{
	@Override
	public void method1()
	{
		System.out.println(" This is overriden abstract method from MyInterface");
	}
	

	public static void main(String[] args) {
		A obj=new A();
		obj.method1();
		MyInterface.method2();
		obj.method3();

	}

}
