package features;
@FunctionalInterface
interface cat{
    void method1();  // abstract method
}

public class LambdaDemo {
//  @Override
//  public void method1() {  //Overriding abstract method from dog
//      System.out.println("Overridden abstract method ");
//  }

	public static void main(String[] args) {
//      LambdaDemo obj=new LambdaDemo();
//       obj.method1();
       
        cat ref=()->System.out.println("Logic....");
        ref.method1();

	}

}
