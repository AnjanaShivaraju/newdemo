package features;
@FunctionalInterface
interface DisplayInterface{
	void display();   //Abstract method
	
}
public class MethodReference3 {
    public void sayHai() { // instance method
    	System.out.println(" Hai....");
    }
	public static void main(String[] args) {
		MethodReference3 obj= new MethodReference3(); // object created
		DisplayInterface D=obj::sayHai; // method references using object
		D.display();// calling the functional interface method
		

	}

}
