package features;

interface MyInterface2{
	void method1();
	static void method2()
	{
		System.out.println(" This is staic method");
	}
}

public class B {

	public static void main(String[] args) {
		 MyInterface2 my=()-> System.out.println("Lambda Expression........");
	        my.method1();   //  calling functional interface method
	}

}
