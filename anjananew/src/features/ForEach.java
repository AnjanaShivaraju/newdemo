package features;

import java.util.Arrays;
import java.util.List;

public class ForEach {

	public static void main(String[] args) {
		List<Integer> number = Arrays.asList(34,33,87,66,12);
		System.out.println(" print integers");
		number.forEach(System.out::println);

	}

}
