package assignment181122;

import java.util.Scanner;

public class PrimeNumber {

	public static void main(String[] args) {
		boolean isPrime=true;
        int temp;
        Scanner sc=new Scanner(System.in);
        System.out.println("enter a number");
        int num=sc.nextInt();
        for(int i=2;i<num/2;i++)
        {
            temp=num%i;
            if(temp==0)
                {
                   isPrime=false;
                }
        }
        if(isPrime)
        
            System.out.println(num + " is a prime number");
        else
            System.out.println(num + " is not a prime number");
	}

}
