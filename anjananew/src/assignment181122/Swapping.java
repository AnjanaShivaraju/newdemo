package assignment181122;

import java.util.Scanner;

public class Swapping {

	public static void main(String[] args) {
		System.out.println("enter the value of a:");
        Scanner sc=new Scanner(System.in);
        int a=sc.nextInt();
        System.out.println("enter the value of b:");
        int b=sc.nextInt();
        int temp;
        temp=a;
        a=b;
        b=temp;
        System.out.println("value of x and y after swapping are: a="+a + "   b="+b);
        
	}

}
