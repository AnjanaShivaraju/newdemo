package mavendemo;

public class Mavendemo1 {
		
			private String name; 
			private int age;
			private String city;
			private String Country;
			
			public String getName() {
			return name;
			}
			public void setName(String name) {
			this.name = name;
			}
			public int getAge() {
			return age;
			}
			public void setAge(int age) {
			this.age = age;
			}
			public String getCity() {
			return city;
			}
			public void setCity(String city) {
			this.city = city;
			}
			public String getCountry() {
				return Country;
				}
			public void setCountry(String Country) {
			this.Country = Country;
				}
			
			public String toString() {
			return "Mavendemo [name=" + name + ", age=" + age + ", city=" + city + ", Country=" + Country +"]";
			}
}


